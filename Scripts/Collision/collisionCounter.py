import sys
import os
import struct
import datetime
import time
import matplotlib.pyplot as plt
import matplotlib
import numpy as np
from scipy.spatial import distance
import pandas as pd

import referee_pb2 as referee
import messages_robocup_ssl_detection_pb2 as detection
import messages_robocup_ssl_geometry_pb2 as geometry
import messages_robocup_ssl_refbox_log_pb2 as refbox
import messages_robocup_ssl_wrapper_pb2 as wrapper


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


FILE_FORMAT_SIZE = 16  # 16 bytes = int64 + 2 x int32

MESSAGE_TYPE = {
    0: "Blank Message",
    1: "Unknown",
    2: "SSL Vision 2010",
    3: "SSL Refbox 2013",
    4: "SSL Vision 2014"
}

teamDict = {}
# Blue, Yellow
robotPos = [np.zeros((6, 2), dtype=np.int16), np.zeros((6, 2), dtype=np.int16)]
nCollisions = 0
lastCollisionTime = 0


def getHeader(f):
    return f.read(12)


def getVersion(f):
    return int.from_bytes(f.read(4), byteorder="big")


def getDecodedMessage(binaryLine):
    # >qii -> format for int64 + int32 + int32 in big-endian
    data = struct.unpack('>qii', binaryLine)
    timestamp = datetime.timedelta(seconds=data[0]/1e9)

    messageType = MESSAGE_TYPE.get(data[1])
    messageSize = data[2]
    # print("Message Size =", messageSize, "Message Type =", messageType,
    #	"Timestamp =", timestamp.split(', ')[-1])

    return timestamp, messageType, messageSize


def getRefereeCommand(frame):
    return frame.command


def getRobotPositions(frame):

    for i in range(0, 6):
        if len(frame.detection.robots_blue) > i+1:
            pos = np.array([frame.detection.robots_blue[i].x,
                            frame.detection.robots_blue[i].y])
            robotPos[0][i] = pos
        if len(frame.detection.robots_yellow) > i+1:
            pos = np.array([frame.detection.robots_yellow[i].x,
                            frame.detection.robots_yellow[i].y])
            robotPos[1][i] = pos
    # print(bcolors.OKBLUE, robotPos[0], bcolors.ENDC)
    # print(bcolors.WARNING, robotPos[1], bcolors.ENDC)


def countCollisions(monitoredID, monitoredTeam, currentTime):
    global nCollisions
    global lastCollisionTime

    if teamDict.__contains__(monitoredTeam):
        teamID = teamDict[monitoredTeam]
    pos = robotPos[teamID][monitoredID]

    for t in range(0, 2):
        for r in range(0, 6):
            if teamID != t or r != monitoredID:
                dist = distance.euclidean(pos, robotPos[t][r])
                if dist <= 190 and currentTime - lastCollisionTime > 0.5:
                    lastCollisionTime = currentTime
                    nCollisions += 1


def getMessageFormat(f):
    return f.read(FILE_FORMAT_SIZE)


def checkFile(f):
    header = getHeader(f)
    version = getVersion(f)
    fileOK = False

    if(header == b'SSL_LOG_FILE'):
        print("File Header", header)
        fileOK = True
    else:
        print("Wrong file header. Found header =", header)
        fileOK = False

    if(version == 1):
        print("File Version =", version)
        fileOK = True & fileOK
    else:
        print("Wrong file version. Found version =", version)
        fileOK = False & fileOK

    return fileOK


def getBallPosition(frame, prevX, prevY):
    if(len(frame.detection.balls) > 0):
        # print("Ball(",frame.detection.balls[0].x,",",frame.detection.balls[0].y,")")
        return frame.detection.balls[0].x, frame.detection.balls[0].y
    return prevX, prevY


def getTeams(refereeFrame, teams, monitoredTeamName):
    if teams == []:
        teams.append(refereeFrame.blue.name)
        teams.append(refereeFrame.yellow.name)

        global teamDict
        teamDict = {teams[0]: 0, teams[1]: 1}

        print("Current teams: ", bcolors.OKBLUE,
              teams[0], bcolors.ENDC, " x ", bcolors.WARNING, teams[1], bcolors.ENDC)
        if not teams.__contains__(monitoredTeamName):
            raise ValueError("Monitored team name not found!")
            return
        input("Press Enter to continue")


def readSSL_Log():
    codeTime = time.time()
    monitoredTeam = ""
    monitoredRobot = -1

    try:
        path = sys.argv[1]
        logFile = open(path, 'rb')
        print("\n", logFile, end="\n\n")
        monitoredTeam = sys.argv[2]
        monitoredRobot = int(sys.argv[3])
    except FileNotFoundError:
        print("Couldn't open file", end="\n\n")
        return
    except IndexError:
        print("Usage: python collisionCounter.py logFile.log monitoredTeamName monitoredRobotID")
        return

    totalBytes = os.path.getsize(path)
    bytesRead = 0

    wrapperFrame = wrapper.SSL_WrapperPacket()
    myRefereeFrame = referee.SSL_Referee()
    teamsOK = False
    refCommando = referee.SSL_Referee.HALT

    if(checkFile(logFile) == True):
        line = getMessageFormat(logFile)
        bytesRead += 32

        teams = []
        remainingPercent = 100

        while line:
            timeStamp, messageType, messageSize = getDecodedMessage(line)
            messageData = logFile.read(messageSize)

            if messageType == "SSL Refbox 2013":
                myRefereeFrame.ParseFromString(messageData)
                getTeams(myRefereeFrame, teams, monitoredTeam)
                refCommando = getRefereeCommand(myRefereeFrame)
                teamsOK = True

            elif messageType == "SSL Vision 2014":
                try:
                    wrapperFrame.ParseFromString(messageData)
                    getRobotPositions(wrapperFrame)
                    if teamsOK and refCommando != referee.SSL_Referee.HALT:
                        countCollisions(monitoredRobot, monitoredTeam,
                                        timeStamp.total_seconds())
                except:
                    pass

            line = getMessageFormat(logFile)
            bytesRead += 16 + messageSize

            auxPercent = round(10000*(totalBytes-bytesRead)/totalBytes)/100

            if(auxPercent < remainingPercent):
                remainingPercent = auxPercent
                print(remainingPercent, "% Remaining -- ", timeStamp, end="\r")

        print("Finished reading log file.", end=" ")
        print("Took ", round(100*time.time() - 100*codeTime)/100, " seconds")
        print("Total collisions:", bcolors.FAIL, nCollisions, bcolors.ENDC)

    logFile.close()


if __name__ == "__main__":
    readSSL_Log()

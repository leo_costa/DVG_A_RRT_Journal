import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import sys
from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})


def acumulate(data):
    """
    Calculates the sum of all data points given.
    """
    acc = 0
    for n, val in enumerate(data):
        acc = acc + val*n
        data[n] = acc


def plotCollisions(data, dataLabels: (str, str)):
    """
    Plot the histogram of the collisions that happened and the total number of
    collisions.
    data -> Data to be plotted.
    dataLabels -> Tuple containing
    """
    maxD = int(max(data[dataLabels[0]].max(),
                   data[dataLabels[1]].max())) + 1
    minD = int(min(data[dataLabels[0]].min(),
                   data[dataLabels[1]].min()))
    bins = np.linspace(minD, maxD, maxD-minD+1)

    data.plot(kind='hist', bins=bins-0.5, alpha=0.7, xticks=bins, grid=True)
    plt.xlabel('# of Collisions per path')
    plt.legend(loc='upper left')

    countA, binA = np.histogram(data[dataLabels[0]], bins=bins, density=False)
    countB, binB = np.histogram(data[dataLabels[1]], bins=bins, density=False)
    acumulate(countA)
    acumulate(countB)

    axis2 = plt.twinx()

    axis2.plot(binA[0:-1], countA, ms=5, alpha=0.7, marker='o', color='C2')
    axis2.plot(binB[0:-1], countB, ms=5, alpha=0.7, marker='o', color='C3')

    axis2.set_ylabel('Total Collisions')
    axis2.grid(False)

    ttlCol = ' Total Collisions'
    axis2.legend([dataLabels[0] + ttlCol, dataLabels[1] + ttlCol],
                 loc='center right')


def plotParam(dataColumn: str, data):
    """
    Plot the box plot of the other parameters.
    dataColumn -> Name of the column to be plotted.
    data -> Data to be plotted.
    """
    meanlineprops = dict(linestyle='solid', linewidth=1.5, color='red')
    data.boxplot(showmeans=True, meanline=True, meanprops=meanlineprops,
                 grid=True)


if __name__ == "__main__":
    dfDados = []
    scenarios = []

    with open(sys.argv[1], 'r') as arqFiles:
        line = arqFiles.readline()
        while line:
            line = line.split(' ')

            dfDados.append(pd.read_csv(line[0], sep=';'))
            line = line[1].split('\n')
            scenarios.append(line[0])
            line = arqFiles.readline()

    scenarios = np.unique(scenarios)
    titles = ['Time [ms]', 'Path Length [m]', 'Recalculated Paths',
              'Time Elapsed [s]', 'Collisions']
    scale = [1, 1e3, 1, 1, 1]
    figNames = ['dynT.png', 'dynPL.png', 'dynRC.png', 'dynNT.png', 'dynCol']
    path = 'Teste2/'
    dataLabels = ['ComputationalTime', 'PathLength', 'TimesRecalculated',
                  'StartGoalTime', 'Colisions']
    algorithms = ['DVG+A*', 'RRT']

    ylabels = ['Time $[ms]$', 'Length $[m]$', '# of Recalculated Paths',
               'Navigation Time $[s]$']

    # Create an nparray with all the data from all experiments.
    completeData = []
    for n in range(0, len(dataLabels)):
        for df in dfDados:
            completeData.append(df[dataLabels[n]].to_numpy()/scale[n])
    completeData = np.array(completeData)

    # Create multi index columns for the data frame.
    columns = pd.MultiIndex.from_product([titles, scenarios, algorithms])
    formatC = '|c'*5

    # Print the latex tables of each parameter.
    completeDf = pd.DataFrame(data=completeData.transpose(), columns=columns)
    for p in titles:
        print(completeDf[p].describe(percentiles=[]).to_latex(
            float_format="%.2f", column_format=formatC,
            multicolumn_format='|c|'))

    topLabels = np.repeat(scenarios, 2)
    xticksLabels = algorithms*2

    for n, param in enumerate(titles):
        if n < 4:
            plotParam(algorithms*2, completeDf[param])
            plt.xticks(range(1, 2*len(algorithms)+1), algorithms*2)
            plt.ylabel(ylabels[n])

            bottomTicks, labels = plt.xticks()
            labels = [l if x == algorithms[0] else ' ' for x, l in
                      zip(xticksLabels,
                          topLabels)]
            twy = plt.twiny()
            twy.set_xticks(bottomTicks)
            twy.set_xticklabels(labels)

            plt.savefig(path + figNames[n])
            # plt.show()
        else:
            for i, s in enumerate(scenarios):
                plotCollisions(completeDf[param][s], tuple(algorithms))
                plt.savefig(path + figNames[n] + str(i) + '.png')
                # plt.show()
        plt.clf()

import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
import numpy as np
import sys

def plotParam(dataColumn : str, dfAux):
    """
    Plot the boxplot of the data.
    dataColumn -> Name of the column to be plotted.
    dfAux -> Pandas dataframe with the data.
    """
    meanlineprops = dict(linestyle='solid', linewidth=1, color='red')
    dfAux.boxplot(showmeans=True, grid=True, column=dataColumn,
            meanline=True, meanprops=meanlineprops)

if __name__ == "__main__":
    dfDados = []
    scenarios = []

    # This script is supposed to receive as argument a text file with the name
    # of the experiments files names.
    with open(sys.argv[1], 'r') as arqFiles:
        line = arqFiles.readline()
        while line:
            line = line.split(' ')

            dfDados.append(pd.read_csv(line[0], sep=';'))
            line = line[1].split('\n')
            scenarios.append(line[0])
            line = arqFiles.readline()

    titles = ['Time [ms]', 'Path Length [m]', 'Distance from Opponents [m]']
    ylabels = ['Time $[ms]$', 'Length $[m]$', 'Distance $[m]$']
    scale = [1,1,1e3,1e3,1e3,1e3]
    dataLabels = ['TempoA', 'TempoRRT', 'ComprimentoA', 'ComprimentoRRT',
                  'InimigosA', 'InimigosRRT']
    algorithms = ['DVG+A*','RRT']

    completeData = []
    # Generate a numpy array containing the data for all experiments.
    for n in range(0, len(dataLabels), 2):
        for df in dfDados:
            completeData.append(df[dataLabels[n]].to_numpy()/scale[n] )
            completeData.append(df[dataLabels[n+1]].to_numpy()/scale[n+1] )
    completeData = np.array(completeData)

    # Create a multi index column for the data.
    columns = pd.MultiIndex.from_product([titles, scenarios, algorithms])

    # Create the dataframe with all the data.
    completeDf = pd.DataFrame(data=completeData.transpose(), columns=columns)

    # Print the latex table for the data.
    print(completeDf.describe(percentiles=[]).to_latex(float_format="%.2f"))
    xticksLabels = algorithms * len(scenarios) * 2

    topLabels = np.repeat(scenarios, 2)

    # Plot
    for n, title in enumerate(titles):
        plotParam(title, completeDf)
        plt.ylabel(ylabels[n])
        plt.xticks(range(1, 2*len(scenarios)+1), xticksLabels)
        bottomTicks, labels = plt.xticks()
        labels = [l if x==algorithms[0] else ' ' for x,l in 
                                                  zip(xticksLabels, topLabels)]
        twy = plt.twiny()
        twy.set_xticks(bottomTicks)
        twy.set_xticklabels(labels)
        plt.subplots_adjust(left=0.09, bottom=0.06, right=0.99, top=0.93,
                        wspace=0.15)
        plt.show()

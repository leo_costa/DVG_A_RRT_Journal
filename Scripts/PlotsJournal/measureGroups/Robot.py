class Robot(object):
    """
    Basic class for the robot data.
    """
    def __init__(self, team='Blue'):
        self.x = 0
        self.y = 0
        self.id = -1
        self.notUpdated = 0
        self.team = team

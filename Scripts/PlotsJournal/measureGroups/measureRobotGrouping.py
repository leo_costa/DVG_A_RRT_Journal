import sys
import os
import LogReader as lgr
import Robot as rbt
import numpy as np
from numpy.linalg import norm as dist
import matplotlib.pyplot as plt
import matplotlib.colors as clrs
from random import uniform
import pandas as pd

N_ROBOTS = 16
GROUP_DIST = 500

colors = ["#ff0000", "#00ff00", "#0000ff", "#00ABF1", "#000000"]

class RobotGrouping(object):
    def __init__(self, logName : str):
        self.fieldSize     = (0,0)
        self.goalA         = np.array([0,0])
        self.goalB         = np.array([0,0])
        self.lastGroup     = []
        self.nGroups       = 0
        self.robots        = []
        self.activeRobots  = []
        self.dfLogGroups   = pd.DataFrame()
        self.robotGroups   = []
        self.logName       = logName

        for i in range(0, N_ROBOTS):
            self.robots.append(rbt.Robot('Yellow'))
        for i in range(0, N_ROBOTS):
            self.robots.append(rbt.Robot('Blue'))

    def setRobots(self, robots : []):
        self.incrementRobotUpdates()
        currentTeam = 0

        for robot in robots:
            if robot.team == 'Blue':
                currentTeam = N_ROBOTS
            else:
                currentTeam = 0

            self.robots[currentTeam + robot.id].id = robot.id
            self.robots[currentTeam + robot.id].x  = robot.x
            self.robots[currentTeam + robot.id].y  = robot.y
            self.robots[currentTeam + robot.id].notUpdated = 0

    def showAllRobots(self):
        for robot in self.robots:
            print(robot.id, robot.team, \
                  '(', round(robot.x), ',', round(robot.y), ')')

    def incrementRobotUpdates(self):
        for robot in self.robots:
            robot.notUpdated += 1
        self.clearRobots()

    def clearRobots(self):
        for robot in self.robots:
            if robot.notUpdated >= 50:
                robot.id = -1
                robot.x  = 0
                robot.y  = 0

    def nRobotsOnField(self) -> int:
        count = 0
        self.activeRobots = []
        for robot in self.robots:
            if robot.id != -1:
                self.activeRobots.append(robot)
                count += 1
        return count

    def detGroups(self) -> bool:
        nObj = self.nRobotsOnField()
        groups = []

        if nObj > 0:
            for c in range(0, nObj):
                while groups.count(c) > 0 and c < nObj - 1:
                    c += 1
                if groups.count(c) == 0:
                    groups.append(c)
                self.findClosest(groups, c, nObj)
                groups.append(-1)

        if groups != self.lastGroup:
            self.lastGroup = groups
            self.nGroups += 1
            # print(self.nGroups, self.lastGroup)
            return True
        else:
            return False

    def findClosest(self, groups : [], c : int, nObj : int):
        for l in range(c + 1, nObj):
            posC = np.array([self.activeRobots[c].x, self.activeRobots[c].y])
            posL = np.array([self.activeRobots[l].x, self.activeRobots[l].y])

            if dist(posC - posL) < GROUP_DIST and dist(posC - posL) > 180 and \
              (dist(posC-self.goalA) > 2e3 and dist(posC-self.goalB) > 2e3) and \
              (dist(posL-self.goalA) > 2e3 and dist(posL-self.goalB) > 2e3):
                if groups.count(l) == 0:
                    groups.append(l)
                self.findClosest(groups, l, nObj)

    def plotCurrentGroup(self):
        maxX = self.fieldSize[0]/1e3
        maxY = self.fieldSize[1]/1e3

        # plt.clf()
        ax = plt.gca()
        del ax.collections[:]

        plt.title('Robot Grouping')
        plt.xlabel('X')
        plt.ylabel('Y')
        plt.grid(True)
        plt.ylim(-maxY/2, maxY/2)
        plt.xlim(-maxX/2, maxX/2)

        group = []
        j = 0
        for i in self.lastGroup:
            if i != -1:
                group.append(i)
            else:
                if len(group) > 0:
                    robotPosX = []
                    robotPosY = []
                    for n in group:
                        robotPosX.append(self.activeRobots[n].x/1e3)
                        robotPosY.append(self.activeRobots[n].y/1e3)
                    plt.scatter(robotPosX, robotPosY, c=colors[j], 
                                s=20, marker='o')
                    # plt.show(block=False)
                    # plt.pause(0.001)
                    plt.savefig("{}/GroupN={}".format('Groups', self.nGroups),
                                dpi=300, bbox_inches='tight')
                    j += 1
                    if j == 5:
                        j = 0

                group.clear()

    def logCurrentGrouping(self):
        group = 0
        for i in self.lastGroup:
            if i != -1:
                group += 1
            else:
                if group > 0:
                    self.robotGroups.append(group)
                    group = 0

    def saveGroupingLog(self):
        # print(self.robotGroups)
        self.dfLogGroups = pd.DataFrame({'Robot Groups' : self.robotGroups})
        self.dfLogGroups.to_csv('Logs/{}.csv'.format(self.logName), sep=";")
        print(self.dfLogGroups.head())
            

if __name__ == '__main__':
    if len(sys.argv) < 4:
        raise(Exception, "Wrong Parameters!! Please provide: sslLogName \
                            groupLogName groupMinDist"
    reader = lgr.LogReader(sys.argv[1])
    groupCalc = RobotGrouping(sys.argv[2])
    GROUP_DIST = int(sys.argv[3])

    while reader.bytesRead < reader.totalBytes:
        print('(', np.around(100.0 * reader.bytesRead / reader.totalBytes, 1),
                '% /', 100, '% )', end='\r')
        tStamp, mType, mData = reader.readLine()

        fieldSize = reader.getFieldSize()
        if fieldSize != (0,0):
            groupCalc.fieldSize = fieldSize
            groupCalc.goalA = np.array([-fieldSize[0]/2, 0])
            groupCalc.goalB = np.array([ fieldSize[0]/2, 0])

        groupCalc.setRobots(reader.getAllRobots())

        if mType != "SSL Vision 2014" and groupCalc.fieldSize != (0,0):
            if groupCalc.detGroups() == True:
                # groupCalc.plotCurrentGroup()
                groupCalc.logCurrentGrouping()
    print('\n')
    groupCalc.saveGroupingLog()

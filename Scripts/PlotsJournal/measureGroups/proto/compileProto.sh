#!/bin/sh

protoc messages_robocup_ssl_detection.proto --python_out=.
protoc messages_robocup_ssl_geometry.proto --python_out=.
protoc messages_robocup_ssl_refbox_log.proto --python_out=.
protoc messages_robocup_ssl_wrapper.proto --python_out=.
protoc referee.proto --python_out=.

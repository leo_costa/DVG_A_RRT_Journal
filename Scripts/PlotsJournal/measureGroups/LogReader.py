import sys
import os
import struct 
import datetime
import Robot as rbt

import referee_pb2 as referee
import messages_robocup_ssl_detection_pb2 as detection
import messages_robocup_ssl_geometry_pb2 as geometry
import messages_robocup_ssl_refbox_log_pb2 as refbox
import messages_robocup_ssl_wrapper_pb2 as wrapper

FILE_FORMAT_SIZE = 16 # 16 bytes = int64 + 2 x int32

MESSAGE_TYPE = {
        0 : "Blank Message",
        1 : "Unknown",
        2 : "SSL Vision 2010",
        3 : "SSL Refbox 2013",
        4 : "SSL Vision 2014"
        }

class LogReader(object):
    """
    Class that reads an SSL Log file.
    """
    def __init__(self, filename : str):
        """
        """
        self.fileOpen   = False
        self.fileOK     = False
        self.bytesRead  = 0
        self.totalBytes = 0
        self.wrapperFrame = wrapper.SSL_WrapperPacket()
        self.refereeFrame = referee.SSL_Referee()

        try:
            self.LogFile    = open(filename,'rb')
            self.fileOpen   = True
            self.totalBytes = os.path.getsize(filename)
            self.checkFile()
            print("\n", self.LogFile, end="\n\n")
        except:
            print("Couldn't open file", end="\n\n")


    def getHeader(self) -> str:
        """
        Return the header of the log file.
        """
        if self.fileOpen == True:
            self.bytesRead += 12
            return self.LogFile.read(12).decode('UTF-8')

    def getVersion(self) -> int:
        """
        Return the version of the log file.
        """
        if self.fileOpen == True:
            self.bytesRead += 4
            return int.from_bytes(self.LogFile.read(4), byteorder = "big") 

    def getMessage(self, binaryLine):
        """
        Returns the timeStamp, messageType and messageSize of the given 
        binaryLine.
        """
          # >qii -> format for int64 + int32 + int32 in big-endian
        data = struct.unpack('>qii',binaryLine) 
        timestamp = datetime.timedelta(seconds=data[0]/1e9)

        messageType = MESSAGE_TYPE.get(data[1])
        messageSize = data[2]
        #print("Message Size =", messageSize, "Message Type =", messageType, 
        #	"Timestamp =", timestamp.split(', ')[-1])
        return timestamp, messageType, messageSize

    def getBinaryData(self):
        """
        Return the current binary packet.
        """
        if self.fileOpen == True:
            self.bytesRead += FILE_FORMAT_SIZE
            return self.LogFile.read(FILE_FORMAT_SIZE)
        raise(ValueError, 'File not open')

    def checkFile(self):
        """
        Checks if the given file is valid.
        """
        header = self.getHeader()
        version = self.getVersion()
        self.fileOK = False

        if(header == 'SSL_LOG_FILE'):
            print("File Header",header)
            self.fileOK = True
        else:
            print("Wrong file header. Found header =", header)
            self.fileOK = False

        if(version == 1):
            print("File Version =",version)
            self.fileOK = True & self.fileOK
        else:
            print("Wrong file version. Found version =", version)
            self.fileOK = False & self.fileOK

    def readLine(self):
        """
        Reads the current log line, decodes it into and referee frame or
        vision frame and returns the timestamp, messageType and messageData.
        """
        if self.fileOK == False or self.fileOpen == False:
            raise(ValueError, 'File not open or not ok')

        binaryData = self.getBinaryData()
        
        if binaryData:
            timeStamp, messageType, messageSize = self.getMessage(binaryData)
            messageData = self.LogFile.read(messageSize)
            self.bytesRead += messageSize

            if messageType == MESSAGE_TYPE[3]:
                # Refbox Message
                self.refereeFrame.ParseFromString(messageData)
            elif messageType == MESSAGE_TYPE[4]:
                # 2014 Vision Message
                self.wrapperFrame.ParseFromString(messageData)

            if __name__ == "__main__":
                print(self.bytesRead, "/", self.totalBytes, " Bytes Read")
            return timeStamp, messageType, messageData

    def getAllRobots(self) -> [rbt.Robot()]:
        """
        Returns an array containing all detected robots.
        """
        robots   = [rbt.Robot()]

        for n in range(0, len(self.wrapperFrame.detection.robots_yellow)):
            auxRobot = rbt.Robot()
            auxRobot.x = self.wrapperFrame.detection.robots_yellow[n].x
            auxRobot.y = self.wrapperFrame.detection.robots_yellow[n].y
            auxRobot.id= self.wrapperFrame.detection.robots_yellow[n].robot_id
            auxRobot.team = 'Yellow'
            robots.append(auxRobot)

        for n in range(0, len(self.wrapperFrame.detection.robots_blue)):
            auxRobot = rbt.Robot()
            auxRobot.x = self.wrapperFrame.detection.robots_blue[n].x
            auxRobot.y = self.wrapperFrame.detection.robots_blue[n].y
            auxRobot.id= self.wrapperFrame.detection.robots_blue[n].robot_id
            auxRobot.team = 'Blue'
            robots.append(auxRobot)
        return robots
    
    def getFieldSize(self) -> (int, int):
        """
        Returns a tuple with the current field size.
        """
        fieldX, fieldY = 0, 0
        fieldX = self.wrapperFrame.geometry.field.field_length
        fieldY = self.wrapperFrame.geometry.field.field_width
        return (fieldX, fieldY)

if __name__ == "__main__":
    logRdr = LogReader('Logs/ERForce-vs-TIGERs.log')
    while logRdr.bytesRead <= logRdr.totalBytes:
        logRdr.readLine()
        logRdr.getAllRobots()

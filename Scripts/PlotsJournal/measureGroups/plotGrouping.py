import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
import numpy as np
import sys

mpl.rcParams.update({'font.size': 30})

def histogramPlot(dfDados, index : int, param : str):
    """
    Histogram plot of the robot grouping.
    dfDados -> Pandas dataframe containing the grouping data.
    index -> Starting index of the data in the dataframe.
    param -> Name of the parameter in the dataframe.
    """
    maxD = 0
    minD = 1000
    dfAux = pd.DataFrame()
    for i in range(0, 4):
        column = dfDados[index+i].columns
        dfAux[column] = dfDados[index+i][column]
        maxD = int(max(maxD, dfAux.iloc[:,-1].max()))
        minD = int(min(minD, dfAux.iloc[:,-1].min()))
    # dfAux.dropna(axis=0, inplace=True)

    bin_edges = np.linspace(minD, maxD+1, maxD-minD+2)
    print(dfAux.describe(percentiles=[]).to_latex(float_format="%.2f"))
    dfAux.plot(kind='hist', bins=bin_edges-0.5, xticks=bin_edges, alpha=0.5,
            logy=True, stacked=True)

    plt.grid(which='major')
    plt.ylabel('Frequency (log)')
    mng = plt.get_current_fig_manager()
    mng.window.maximize()

if __name__ == "__main__":
    dfDados = []
    
    for i in range(1, 5):
        dfDados.append(pd.read_csv('Logs/Dist500/' + sys.argv[i], 
                       sep=';', index_col=0))
    for i in range(1, 5):
        dfDados.append(pd.read_csv('Logs/Dist1500/' + sys.argv[i], 
                       sep=';', index_col=0))

    dgroups = ['500 mm', '1500 mm']
    for n,i in enumerate([0,4]):
        histogramPlot(dfDados, i, 'Robot Groups')
        # plt.title('Robot Grouping | $d_{group}$'+'$={}$'.format(dgroups[n]))
        plt.xlabel('Group Size')
        plt.subplots_adjust(left=0.07, bottom=0.1, right=0.99, top=0.995)
        # n = dgroups[n].split()
        plt.show()
        # plt.savefig('Grouping{}.png'.format(n[0]))

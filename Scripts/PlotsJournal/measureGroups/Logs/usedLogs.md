# The logs used to measure the robot grouping were from the following matches:

* ERForce vs TIGERS - 2019
* RoboCIn vs NEUIslanders - 2019
* RoboFEI vs UBSThunderbots - 2019
* ZJUNlict vs MRL - 2019

They can be found [here](https://ssl.robocup.org/game-logs/).

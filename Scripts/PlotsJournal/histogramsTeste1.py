import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
import numpy as np
import sys

def plotParam(paramName : str, dfAux):
    """
    Plot the histogram of the given data.
    paramName -> Name of the parameter to be plotted.
    dfAux -> Data frame containing the data.
    """
    # print(paramName, '\n', 
            # dfAux.describe(percentiles=[]).to_latex(float_format="%.2f"), '\n')

    # meanlineprops = dict(linestyle='solid', linewidth=1, color='red')
    # dfAux.plot(kind='box', showmeans=True, grid=True, figsize=(4.5,3), 
            # meanline=True, meanprops=meanlineprops)
    count, bins = np.histogram(dfAux.iloc[:,:], bins=8)
    dfAux.plot(kind='hist', bins=bins, grid=True, figsize=(4.5,4), alpha=0.5)
    plt.xticks(bins, np.around(bins, decimals=1))

if __name__ == "__main__":
    dfDados = pd.read_csv(sys.argv[1], sep=';')
    titles = ['Time [ms]', 'Path Length [m]', 'Distance from Opponents [m]']
    ylabels = ['Time $[ms]$', 'Length $[m]$', 'Distance $[m]$']

    # dfDados['TempoDVG'] = dfDados['TempoA'] 

    columns = pd.MultiIndex.from_product([titles, ['DVG+A*', 'RRT']])
    completeData = np.array([dfDados['TempoA'],
                    dfDados['TempoRRT'],
                    dfDados['ComprimentoA']/1e3,
                    dfDados['ComprimentoRRT']/1e3,
                    dfDados['InimigosA']/1e3,
                    dfDados['InimigosRRT']/1e3])
    completeDf = pd.DataFrame(data=completeData.transpose(), columns=columns)
    print(completeDf.describe(percentiles=[]).to_latex(float_format="%.2f"))

    for n, title in enumerate(titles):
        plotParam(titles[n], completeDf[titles[n]])
        plt.xlabel(ylabels[n])
        plt.subplots_adjust(left=0.15, bottom=0.15, right=0.99, top=0.99,
                        wspace=0.15)
    plt.show()

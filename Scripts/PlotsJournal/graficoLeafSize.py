import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
import numpy as np
import sys

mpl.rcParams.update({'font.size': 18})

def plotParam(paramName : str, data):
    """
    Plots a boxplot of the given parameter.
    paramName -> Parameter to be plotter.
    data -> Data to be plotted.
    """
    dfAux = pd.DataFrame()
    for n in range(0, len(data)):
        simplifiedLabel = data[n].columns[9].split(' = ')
        if paramName == 'ComprimentoRRT':
            dfAux[simplifiedLabel[-1]] = data[n][paramName]/1e3
        else:
            dfAux[simplifiedLabel[-1]] = data[n][paramName]
    statsData = dfAux.describe()

    print(paramName, '\n', statsData.to_latex(), '\n\n')

    meanlineprops = dict(linestyle='solid', linewidth=1, color='red')
    dfAux.boxplot(showmeans=True, meanline=True, meanprops=meanlineprops,
                figsize = (9,6), grid=True)

if __name__ == "__main__":
    dfDados = []
    leafSizes = []

    # This script is supposed to receive as argument a text file with the name
    # of the experiments files names.
    with open(sys.argv[1], 'r') as arqFiles:
        line = arqFiles.readline()
        while line:
            line = line.split('\n')

            dfDados.append(pd.read_csv(line[0], sep=';'))
            line = arqFiles.readline()
            leafSizes.append(dfDados[-1].columns[9])
    print(leafSizes)

    parameters = ['TempoRRT', 'ComprimentoRRT']
    titles = ['Computational Time', 'Path Length']
    ylabels = ['Time $[ms]$', 'Length $[m]$']
    arq = ['Time.png', 'Path.png']


    for n, title in enumerate(titles):
        plotParam(parameters[n], dfDados)
        # plt.title(title)
        plt.ylabel(ylabels[n])
        plt.xlabel('Leaf Size')
        plt.subplots_adjust(left=0.11, right=0.99, top=0.94, bottom=0.10)
        plt.savefig('TestesLeafSize/leafSizeTest' + arq[n]) 
        plt.show()
